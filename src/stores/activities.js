export const activities = [
  {
    video:'sports',
    title:'Sports',
    desc:'We believe in making it as easy as possible to play team sports. So you can dive right into a kickabout with our football-loving crowd or shoot some hoops if basketball is your thing – all without worrying about where and when and who brings the ball.'
  },
  {
    video:'activities',
    title:'Activities',
    desc:'To keep your mind in focus you need to take it off work at times. Take a break and challenge a co-worker to a game of table football or novuss, level up on your fave Xbox game, join the board game geeks, or give the punching bag a right bashing.'
  },  
  {
    video:'lab',
    title:'Labs',
    desc:'Tinkering with tech is in our DNA. You can flex those welding muscles at our electronics gym, nerd out with the 3D printers or drones, and devise your own laws of robotics with the robots we have around.'
  },
]