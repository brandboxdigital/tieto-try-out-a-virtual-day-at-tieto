import { writable } from 'svelte/store'

// Variables
const vars = {
  scope: 'NEUTRAL', // NEUTRAL
  seat: '',
  skipedWork: false,
  skipedFreeTime: false,
}

// YouTube video embed codes
const codes = {
  'F0': { FIN: 'F5A1B_FIN', IT: 'F0_IT' }, // F5A1B FIN / F0 IT

  'F1': { FIN: 'F1_FIN', IT: 'F1_IT' }, // F1 IT
  'F1B': { FIN: 'F1B_FIN', IT: 'F1B_IT' }, // F1B IT
  'FA': { FIN: 'F4A_FIN', IT: 'FA_IT' }, // F4A FIN / FA IT
  'FB': { FIN: 'F4B_FIN', IT: 'FB_IT' }, // F4B FIN / FB IT

  'FA1': { FIN: 'F4A1_FIN', IT: 'FA1_IT' }, // F4A1 FIN / FA1 IT
  'FB1': { FIN: 'F4B1_FIN', IT: 'FB1_IT' }, // F4B1 FIN / FB1 IT

  'F2': { FIN: 'F2_FIN', IT: 'F2_IT' }, // F2 IT
  'FA2': { FIN: '---', IT: 'FA2_IT' }, // - / FA2 IT
  'FB2': { FIN: '---', IT: 'FB2_IT' }, // - / FB2 IT

  'F3': { FIN: '---', IT: '---' },
  'F3A': { FIN: 'F3A_FIN', IT: 'F3A_IT' }, // F3A FIN / F3A IT
  'F3A1': { FIN: 'F3A1_FIN', IT: 'F3A1_IT' }, // F3A1 FIN / F3A1 IT
  'F3A2': { FIN: 'F3A2_FIN', IT: 'F3A2_IT' }, // F3A2 IT
  'F3B': { FIN: 'F3B_FIN', IT: 'F3B_IT' }, // F3B FIN / F3B IT
  'F3B1': { FIN: 'F3B1_FIN', IT: 'F3B1_IT' }, // F3B1 FIN / F3B1 IT
  'F3C': { FIN: 'F3C_FIN', IT: 'F3C_IT' }, // F3C FIN / F3C IT
  'F3C1': { FIN: 'F3C1_FIN', IT: 'F3C1_IT' }, // F3C1 FIN / F3C1 IT

  'F5A': { FIN: 'F5A_FIN', IT: 'F5A_IT' }, // F5A FIN / F5A IT
  'F5A1': { FIN: 'F5A1_FIN', IT: 'F5A1_IT' }, // F5A1 FIN / F5A1 IT
  'F5A1A': { FIN: 'F5A1A_FIN', IT: 'F5A1A_IT' }, // F5A1A FIN / F5A1A
  'F5A2A': { FIN: 'F5A2A_FIN', IT: 'F5A2A_IT' }, // F5A2A FIN / F5A2A
  'F5A2': { FIN: 'F5A2_FIN', IT: 'F5A2_IT' }, // F5A2 FIN / F5A2 IT
  'F5B': { FIN: 'F5B_FIN', IT: 'F5B_IT' }, // F5B FIN / F5B IT
  'F5B1': { FIN: 'F5B1_FIN', IT: 'F5B1_IT' }, // F5B1 FIN / F5B1 IT
  'F5C': { FIN: 'F5C_FIN', IT: 'F5C_IT' }, // F5C FIN / F5C IT
  'F5C1': { FIN: 'F5C1_FIN', IT: 'F5C1_IT' }, // F5C1 FIN / F5C1 IT

  'F6A': { FIN: 'F6A_FIN', IT: 'F6A_IT' }, // F6A FIN / F6A IT
  'F6A1': { FIN: 'F6A1_FIN', IT: 'F6A1_IT' }, // F6A1 FIN / F6A1 IT
  'F6B': { FIN: 'F6B_FIN', IT: 'F6B_IT' }, // F6B FIN / F6B IT
  'F6B1': { FIN: 'F6B1_FIN', IT: 'F6B1_IT' }, // F6B1 FIN / F6B1 IT
  'F6C': { FIN: 'F6C_FIN', IT: 'F6C_IT' }, // F6C FIN / F6C IT
  'F6C1': { FIN: 'F6C1_FIN', IT: 'F6C1_IT' }, // F6C1 FIN / F6C1 IT

  'F7': { FIN: 'F7_FIN', IT: 'F7_IT' }, // F7 FIN / F7 IT

  'F8': { FIN: 'F8_FIN', IT: 'F8_IT' }, // F8 FIN / F8 IT
}

// Scenario
const scenario = {
  nStart: {
    questions: {
      NEUTRAL: 'WOULD YOU LAST A DAY AT TietoEVRY?',
    },
    type: 'buttons',
    options: [
      { names: { NEUTRAL: 'FINANCE' }, fn: () => { vars.scope = 'FIN'; status.set(scenario.nWelcome) } },
      { names: { NEUTRAL: 'IT' }, fn: () => { vars.scope = 'IT'; status.set(scenario.nWelcome) } },
    ],
  },
  nWelcome: {
    questions: {
      IT: 'You arrive at the office. It’s a wonderful day at TietoEVRY, shall we get to work?',
      FIN: 'You arrive at the office. It’s a wonderful day at TietoEVRY, shall we get to work?',
    },
    type: 'buttons',
    options: [
      {
        names: { IT: 'Let\'s get started!', FIN: 'Let\'s get started!' },
        fn: () => {
          status.set(scenario.nStartVideo)
        }
      },
    ],
  },
  nStartVideo: {
    type: 'video',
    node: 'F1',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nIntro) },
  },
  nIntro: {
    questions: {
      IT: 'Follow the HR lady?',
      FIN: 'Follow the HR lady?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Accept', FIN: 'Accept' }, fn: () => { status.set(scenario.nGotoScreen) } },
      { names: { IT: 'Refuse', FIN: 'Refuse' }, fn: () => { status.set(scenario.nPrank) } },
    ],
  },
  nPrank: {
    type: 'video',
    node: 'F1B',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nPrankFinish) },
  },
  nPrankFinish: {
    questions: {
      IT: 'Is this a prank? Try again.',
      FIN: 'Is this a prank? Try again.',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Oh, you prankster…', FIN: 'Oh, you prankster…' }, fn: () => { status.set(scenario.nStartVideo) } },
    ],
  },

  nGotoScreen: {
    type: 'video',
    node: 'F2',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nSeat) },
  },
  nSeat: {
    questions: {
      IT: 'Which seat will you take?',
      FIN: 'Which seat will you take?',
    },
    type: 'buttons',
    //map:true,
    options: [
      { names: { IT: 'Honeycomb', FIN: 'Honeycomb' }, fn: () => { vars.seat = 'A'; status.set(scenario.nSeatPlace) } },
      { names: { IT: 'Lounge', FIN: 'Lounge' }, fn: () => { vars.seat = 'B'; status.set(scenario.nSeatPlace) } },
      { names: { IT: 'Standing desk', FIN: 'Standing desk' }, fn: () => { vars.seat = 'C'; status.set(scenario.nSeatPlace) } },
    ],
  },
  nSeatPlace: {
    type: 'video',
    node: 'F3{A,B,C}',
    onLoad: () => { scenario.nSeatPlace.node = 'F3' + vars.seat },
    onFinish: () => { status.set(scenario.nJob) },
  },
  nJob: {
    questions: {
      IT: 'New task: Code troubleshooting. Start now or coffee first?',
      FIN: 'New task: Sorting invoices. Start now or coffee first?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Start the job!', FIN: 'Start now!' }, fn: () => { status.set(scenario.nJobTimelapse) } },
      { names: { IT: 'Coffee first!', FIN: 'Coffee first!' }, fn: () => { status.set(scenario.nCoffeeBreak) } },
    ],
  },
  nCoffeeBreak: {
    type: 'video',
    node: 'F3A2',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nJobTimelapse) },
  },
  nJobTimelapse: {
    type: 'video',
    node: 'F3{A,B,C}1',
    onLoad: () => { scenario.nJobTimelapse.node = 'F3' + vars.seat + '1' },
    onFinish: () => { status.set(scenario.nPlayTime) },
  },
  nPlayTime: {
    questions: {
      IT: 'Time to relax - pick what you want!',
      FIN: 'Time to relax - pick what you want!',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Coffee', FIN: 'Walk around' }, fn: () => { status.set(scenario.nWalkAround) } },
      { names: { IT: 'Electronics gym', FIN: 'Punching bag' }, fn: () => { status.set(scenario.nElectronicsGym) } },
    ],
  },

  nWalkAround: {
    type: 'video',
    node: 'FA',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nSelectMilk) },
  },
  nSelectMilk: {
    questions: {
      IT: 'Which milk do you prefer?',
      FIN: 'Go grab some fresh fruits?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Regular', FIN: 'Yeah, sure!' }, fn: () => { status.set(scenario.nMilkRegular) } },
      { names: { IT: 'Soy', FIN: 'Sorry, got to work!' }, fn: () => { status.set(vars.scope == "IT" ? scenario.nMilkSoy : scenario.nNewTaskVideo) } },
    ],
  },
  nMilkRegular: {
    type: 'video',
    node: 'FA1',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nNewTaskVideo) },
  },
  nMilkSoy: {
    type: 'video',
    node: 'FA2',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nNewTaskVideo) },
  },

  nElectronicsGym: {
    type: 'video',
    node: 'FB',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.n3Dprinter) },
  },
  n3Dprinter: {
    questions: {
      IT: 'Help out the guys?',
      FIN: 'The force is strong! Grab a drink before getting back?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Yeah!', FIN: 'Grab a drink!' }, fn: () => { status.set(scenario.n3Dyes) } },
      { names: { IT: 'Sorry, can’t!', FIN: 'Back to work!' }, fn: () => { status.set(vars.scope == "IT" ? scenario.n3Dno : scenario.nNewTaskVideo) } },
    ],
  },
  n3Dyes: {
    type: 'video',
    node: 'FB1',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nNewTaskVideo) },
  },
  n3Dno: {
    type: 'video',
    node: 'FB2',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nNewTaskVideo) },
  },

  nNewTaskVideo: {
    type: 'video',
    node: 'F5{A,B,C}',
    onLoad: () => { scenario.nNewTaskVideo.node = 'F5' + vars.seat },
    onFinish: () => { status.set(scenario.nNewTask) },
  },
  nNewTask: {
    questions: {
      IT: 'Task: App module programming. Work now or go for a quick game?',
      FIN: 'New task: Bank account reconciliation for Norway unit. Work now or game first?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Work', FIN: 'Work' }, fn: () => { status.set(scenario.nWorkNow) } },
      {
        names: { IT: 'Game', FIN: 'Game' }, fn: () => {
          vars.skipedWork = true
          status.set(scenario.nWorkAfter)
        }
      },
    ],
  },

  nWorkNow: {
    type: 'video',
    node: 'F5{A,B,C}1',
    onLoad: () => { scenario.nWorkNow.node = 'F5' + vars.seat + '1' },
    onFinish: () => { status.set(scenario.nWorkDone) },
  },
  nWorkDone: {
    questions: {
      IT: 'Nice tempo! How are we going to relax?',
      FIN: 'Nice tempo! How are we going to relax?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Game', FIN: 'Game' }, fn: () => { status.set(scenario.nRacingGame) } },
      { names: { IT: 'Coffee with mates', FIN: 'Massage chair' }, fn: () => { status.set(scenario.nCoffeeMates) } },
    ],
  },
  nRacingGame: {
    type: 'video',
    node: 'F5A1A',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nFinishPlaying) },
  },
  nCoffeeMates: {
    type: 'video',
    node: 'F0',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nFinishPlaying) },
  },

  nWorkAfter: {
    type: 'video',
    node: 'F5A2',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nNewChallange) },
  },
  nNewChallange: {
    questions: {
      IT: 'Rematch?',
      FIN: 'Rematch?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Sure!', FIN: 'Bring it!' }, fn: () => { status.set(scenario.nBrintIt) } },
      { names: { IT: 'Get back to work!', FIN: 'Get back to work!' }, fn: () => { status.set(scenario.nFinishPlaying) } },
    ],
  },
  nBrintIt: {
    type: 'video',
    node: 'F5A2A',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nFinishPlaying) },
  },
  nFinishPlaying: {
    type: 'video',
    node: 'F6{A,B,C}',
    onLoad: () => { scenario.nFinishPlaying.node = 'F6' + vars.seat },
    onFinish: () => { status.set(scenario.nWorkContinue) },
  },
  nWorkContinue: {
    questions: {
      IT: 'App development or ATM payment systems?',
      FIN: 'Legal documentation or market analysis?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'App', FIN: 'Legal' }, fn: () => { status.set(scenario.nManager) } },
      { names: { IT: 'ATM', FIN: 'Analysis' }, fn: () => { status.set(scenario.nManager) } },
    ],
  },

  nManager: {
    type: 'video',
    node: 'F6{A,B,C}1',
    onLoad: () => { scenario.nManager.node = 'F6' + vars.seat + '1' },
    onFinish: () => { status.set(scenario.nOptions) },
  },

  nOptions: {
    questions: {
      IT: 'Splendid job! Day is almost done, would you like to do something fun?',
      FIN: 'Splendid job! Day is almost done, would you like to do something fun?',
    },
    type: 'buttons',
    options: [
      { names: { IT: 'Football', FIN: 'Basketball' }, fn: () => { status.set(scenario.nFootball) } },
      {
        names: { IT: 'Skip', FIN: 'Skip' }, fn: () => {
          vars.skipedFreeTime = true
          status.set(scenario.nFinish)
        }
      },
      { names: { IT: 'Board game', FIN: 'Board game' }, fn: () => { status.set(scenario.nBoardgame) } },
    ],
  },

  nFootball: {
    type: 'video',
    node: 'F7',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nFinish) },
  },
  nBoardgame: {
    type: 'video',
    node: 'F8',
    onLoad: () => { },
    onFinish: () => { status.set(scenario.nFinish) },
  },

  nFinish: {
    questions: {
      IT: 'Hope you had a blast at TietoEVRY, let’s see how you did on your first day!',
      FIN: 'Hope you had a blast at TietoEVRY, let’s see how you did on your first day!',
    },
    type: 'results',
    onLoad: () => { loadFinish() },
  },
}

const loadFinish = () => {
  let finalResult = 'You’re perfect! The balance between work and play is strong with you! Shall we arrange a meeting?'
  if (vars.skipedWork) finalResult = 'Looks like you\'ve got some unfinished tasks. Getting stuff done on time is a must, let’s work on that!'
  if (vars.skipedFreeTime) finalResult = 'All work and no play? As much as we envy that, it’s a must to take a break from time to time.'

  scenario.nFinish.questions.IT = finalResult
  scenario.nFinish.questions.FIN = finalResult
}

// Store
const status = writable(scenario.nStart) // nStart

// Functions
const getVideoID = node => {
  return '/res/video/'+vars.scope+'/'+getRes()+'/'+codes[node][vars.scope]+'.mp4';
}
const restart = () => {
  // Set to default values
  vars.scope = 'NEUTRAL'
  vars.seat = ''
  vars.skipedWork = false
  vars.skipedFreeTime = false

  status.set( scenario.nStart )
}

const getRes = () => {
  if (window.innerWidth >= 1600 ) {
    return '1080';
  } else if (window.innerWidth >= 1060) {
    return '720';
  } else {
    return '480';
  }
}

// Export
export default {
  loadNextVideoStep: (node) => {
    return scenario[node]
  },
  getVideoID,
  status,
  vars,
  restart,
}
