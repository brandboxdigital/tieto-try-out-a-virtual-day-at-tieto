"use strict";

//
// DOCS: https://developers.google.com/youtube/iframe_api_reference
//

// YouTube embedding script
window.addEventListener('DOMContentLoaded', function (event) {
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); // To access YT player

  console.log(`DOM loaded and parsed`)
});

var youtubePlayer; // YouTube default API Ready function (do not rename it!)

function onYouTubeIframeAPIReady() {
  console.log(`YT API fired!`,document.querySelector('#youtube-player-element'))
  new YT.Player('youtube-player-element', {
    width: window.innerWidth * 0.8,
    height: 3000,
    videoId: '',
    playerVars: {
      'autoplay': 1,
      'controls': 0,
      'color': 'white',
      'iv_load_policy': 3,
      // do not show annotations
      'modestbranding': 1,
      // do not show yt logo
      'playsinline': 1,
      'rel': 0,
      'enablejsapi': 1
    },
    events: {
      'onReady': YouTube.ready,
      'onStateChange': YouTube.stateChange
    }
  }); //console.log( youtubePlayer.loadVideoById )
} // Our controller class for YouTube


var YouTube = {
  isReady: false,
  waitingVideoToLoad:'',
  callOnFinish: null,
  loadVideo: function loadVideo(id) {
    console.log(`Loading video...`);
    if( !YouTube.isReady ){
      console.log(`YT is not ready... loading in idle.`);
      YouTube.waitingVideoToLoad = id;
    }else{
      console.log(`YT is ready! Loading video now!`);
      youtubePlayer.loadVideoById(id);
    }
  },
  ready: function ready(e) {
    console.log(`API loaded`);

    youtubePlayer = e.target;
    YouTube.isReady = true;

    if( YouTube.waitingVideoToLoad != '' ){
      console.log(`Video is in idle... loading now!`);
      youtubePlayer.loadVideoById(id);
    }
  },
  stateChange: function stateChange(event) {
    console.log(`stateChange`,event);
    if (event.data == 0) {
      YouTube.callOnFinish();
    }
  }
};
window.addEventListener('resize', function (e) {
  var newWidth = window.innerWidth;
  document.querySelector('#youtube-player-element').setAttribute('width', newWidth);
});

console.log(`YT embeding`);